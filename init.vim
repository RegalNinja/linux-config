" Setting up plugins and color schemes
call plug#begin('~/.vim/plugged')

    Plug 'https://github.com/xolox/vim-misc'
    Plug 'https://github.com/xolox/vim-notes'
    Plug 'junegunn/goyo.vim'

call plug#end()

" Set the current color scheme
" colorscheme VisualStudioDark

" Bindings
vnoremap <C-c> "*Y :let @+=@*<CR>
map <C-v> "+P

" Setting up hybrid numbers
set number relativenumber

:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

set ruler

set shiftwidth=4    " Number of spaces to use for each step of (auto)indent.

set incsearch       " While typing a search command, show immediately where the
                    " so far typed pattern matches.

set autoindent      " Copy indent from current line when starting a new line
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab





" auto detect filetype
filetype plugin on
